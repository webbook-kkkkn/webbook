<html>

<body>
<?php
$query = 'select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where id_ogloszenie="' . $params[0] . '" limit 1;';
$action = 'szczegoly';
if($q = $db->query($query)){
	$q_array = $q->fetch_array(MYSQLI_ASSOC);
	$nazwa = $q_array['nazwa'];
	$opis = $q_array['opis'];
	$cena = $q_array['cena'];
	$nazwa_kategorii = $q_array['nazwa_kategorii'];
	$data_wystawienia = $q_array['data_wystawienia'];
	$uzytkownik = $q_array['username'];
	$email = $q_array['email'];
	$id = $q_array['id_ogloszenie'];
	$miasto = $q_array['miasto'];
	$wojewodztwo = $q_array['nazwa_wojewodztwa'];
	$dataPolska = convertDate($data_wystawienia);
	$userid = $q_array['fk_id_user'];
}
?>
<article class="ui piled segment">
	<h1 class="ui header"><?php echo $nazwa; ?></h1>
	<?php
	/*echo '<form method="POST" class="ui form" enctype="multipart/form-data" action="./zapytajoogloszenie/'.$id.'">';
	echo '<button type="submit" class="positive ui labeled icon button"><i class="pencil icon"></i>Zapytaj o przedmiot</button>';
	echo '</form>';*/
	if($zalogowany && $_SESSION['admin'] == 1){
		echo '<a href="edytuj/' . $id . '" class="ui icon button right floated"><i class="pencil icon"></i></a>';
	}
	?>
	<!--<h4 class="sub header">Województwo: <?php //echo $wojewodztwo; ?> </h4>-->
	<h4 class="sub header">Autor: <?php echo $miasto; ?> </h4>
	<h4 class="sub header">Gatunek: <?php echo $nazwa_kategorii; ?></h4>
	<?php
	if($cena == '0')
		echo '<h4 class="sub header">Za darmo</h4>';
	else
		echo '<h4 class="sub header">Cena: ' . $cena . 'zł</h4>';
	?>
	<!--<h4 class="sub header">Dodano: <?php //echo $dataPolska; ?></h4>-->
	<div class="sub header"> Przez: <?php echo ' <a href="./userprofil/'.$userid.'">' . $uzytkownik . ' (<a href="mailto:' . $email . '">' . $email . '</a>)</div>';?>
	<h4 class="sub header">Opis książki:</h4>

	<p><?php echo $opis; ?></p>

	<!-- ------------------------------------------------------------------------------------------------------------------ -->
	<?php

if($zalogowany)
{
	$like_query = 'SELECT count(*) as czydal FROM oceny_ksiazki where id_ksiazki="'.$params[0].'" and id_usera="'.$_SESSION['id_uzytkownika'].'" and ocena="1"';
	$dislike_query = 'SELECT count(*) as czydal FROM oceny_ksiazki where id_ksiazki="'.$params[0].'" and id_usera="'.$_SESSION['id_uzytkownika'].'" and ocena="-1"';
	$z_l = $db->query($like_query);
	$likes = $z_l->fetch_array();
	$z_dl = $db->query($dislike_query);
	$dislikes = $z_dl->fetch_array();
	$dal_like = $likes['czydal'];
	$dal_dislike = $dislikes['czydal'];
	$dal = $dal_like + $dal_dislike;
}
if(isset($params[1]) && $params[1] == 'addlike')
{
	if($dal == 0)
	{
		$query_likes = 'INSERT INTO oceny_ksiazki  VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", 1)';
		$db->query($query_likes);
	}
	else
	{
		$db->query('delete from oceny_ksiazki where id_usera="' . $_SESSION['id_uzytkownika'] . '" and id_ksiazki="' . $params[0] . '";');
		if($dal_dislike == 1)
		{
			$query_likes = 'INSERT INTO oceny_ksiazki VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", 1)';
			$db->query($query_likes);
		}
	}

	header('Location: /webbook/szczegoly/' . $params[0]);
}

if(isset($params[1]) && $params[1] == 'addunlike')
{
	if($dal == 0)
		{
			$query_unlikes = 'INSERT INTO oceny_ksiazki VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", -1)';
			$db->query($query_unlikes);
		}
		else
		{
				$db->query('delete from oceny_ksiazki where id_usera="' . $_SESSION['id_uzytkownika'] . '" and id_ksiazki="' . $params[0] . '";');
				if($dal_like == 1)
				{
				$query_unlikes = 'INSERT INTO oceny_ksiazki VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", -1)';
				$db->query($query_unlikes);
			}
		}

		header('Location: /webbook/szczegoly/' . $params[0]);
}
if($zalogowany && $dal_like == 0)//zalogowany bez like  
{
	echo '<a class="ui labeled button" tabindex="0" href="./szczegoly/'.$params[0].'/addlike">
	<div class="ui grey button">
    <i class="heart icon"></i> Lubi
  </div>
  <div class="ui basic grey left pointing label"">
	';
}
else if($zalogowany) //zalogowany z like
{
	echo '<a class="ui labeled button" tabindex="0" href="./szczegoly/'.$params[0].'/addlike">
	<div class="ui red button">
		<i class="heart icon"></i> Polubiono
	</div>
	<div class="ui basic red left pointing label"">
';
}
else //niezalogowany
{
	echo '<a class="ui disabled labeled button" tabindex="0" href="./szczegoly/'.$params[0].'/addlike">
	<div class="ui grey button">
		<i class="heart icon"></i> Lubi
	</div>
	<div class="ui basic grey left pointing label"">
';
}

	//ilosc łapek w gore
	$lajki = $db->query('select count(*)  as c from oceny_ksiazki where id_ksiazki ="' . $params[0] . '" and ocena="1" limit 1;');
	$i = $lajki->fetch_array(MYSQLI_ASSOC);
	$ilosc_lajkow = $i['c'];
	echo $ilosc_lajkow;
	?>
</div>
</a>
<br />
<br />
<?php
if($zalogowany && $dal_dislike== 0) //
	{
		echo '<a class="ui labeled button" tabindex="0" href="./szczegoly/'.$params[0].'/addunlike">
		<div class="ui grey button">
			<i class="thumbs down icon"></i> Nie lubi
		</div>
		<div class="ui basic grey left pointing label"">
';
	}
	else if($zalogowany)
	{
		echo '<a class="ui labeled button" tabindex="0" href="./szczegoly/'.$params[0].'/addunlike">
		<div class="ui red button">
			<i class="thumbs down icon"></i> Nie polubiono
		</div>
		<div class="ui basic red left pointing label"">
';

	}
else {
	echo '<a class="ui disabled labeled button" tabindex="0" href="./szczegoly/'.$params[0].'/addunlike">
	<div class="ui grey button">
		<i class="thumbs down icon"></i> Nie lubi
	</div>
	<div class="ui basic grey left pointing label"">
';

}
	//ilosc lapek w dol
	$unlajki = $db->query('select count(*)  as u from oceny_ksiazki where id_ksiazki="' . $params[0] . '" and ocena="-1" limit 1;');
	$u = $unlajki->fetch_array(MYSQLI_ASSOC);
	$ilosc_unlajkow = $u['u'];
	echo $ilosc_unlajkow;
	?>
</div>
</a>
<br />
	<!-- ------------------------------------------------------------------------------------------------------------------ -->
	<?php
	if(isArticleImage($id))
	{
		$directory = 'images/ogloszenia/' . $id . '/';
		//$images = scandir($directory);
		$images = array_diff(scandir($directory), array('.', '..', 'thumbnail'));
		echo '<article class="ui piled segment">';
		echo '<h1 class="ui header">Galeria zdjęć</h1>';
	echo '<div class="ui small images galeria">';
		foreach($images as $image)
		{
		  //echo '<a href="' . $directory . $image . '">';
			echo '<img data-src="'.$directory.$image.'" src="' . $directory;
			if(is_file($directory . 'thumbnail/' . $image)) echo 'thumbnail/';
			echo $image . '"/>';
			//echo '</a>';
		}
	echo '</div>';

	?>
	<div class="container">
		<span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
		<img id="expandedImg" style="width: 100%">
		<div id="imgtext"></div>
	</div>
	<?php
		}
	?>
</article>

<script>
function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  var imgText = document.getElementById("imgtext");
  expandImg.src = imgs.src;
  imgText.innerHTML = imgs.alt;
  expandImg.parentElement.style.display = "block";
}
</script>
</body>
</html>
