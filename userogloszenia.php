<div class="ui two column stackable grid">
<?php
	$id = $params[0];
	$strona = !empty($params[1]) ? $params[1] : 1;
	$ilosc_na_strone = 6;

	$wszystkie_ogloszenia = $db->query('select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria = kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where ogloszenie.fk_id_user="' . $id . '";');
	$w = $wszystkie_ogloszenia->num_rows;
	$wszystkie = $w;

	$ilosc_stron = ceil($wszystkie / $ilosc_na_strone);
	$ogloszenia = $db->query('select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria = kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where ogloszenie.fk_id_user="' . $id . '" order by data_wystawienia desc, id_ogloszenie desc limit ' . (($strona - 1) * $ilosc_na_strone) . ', ' . ($ilosc_na_strone));
	include 'ogloszenia.php';

	$poprzednia = $strona -1 ;
	$nastepna = $strona + 1;
?>
<div class="one column row" >
<div class="ui pagination menu grid container">
<?php
	if($poprzednia > 0){
		echo '<a href="userprofil/'. $id . '/' . $poprzednia .'" class="item left aligned">Poprzednia</a>';
	}
	else
	{
		echo '<div class="item left disabled aligned">Poprzednia</div>';
	}

	for($i = 0; $i < $ilosc_stron; $i++){
		$a = ($i + 1);
		echo '<a href="userprofil/'. $id . '/' . $a . '" class="item one wide column';
		if($strona == $a) echo ' active';
		echo'">' . $a . '</a>&nbsp;';
	}

	if ($nastepna <= $ilosc_stron){
		echo '<a href="userprofil/'. $id . '/' . $nastepna .'" class="item right aligned">Nastepna</a>';
	}
	else
	{
		echo '<div class="item right disabled aligned">Następna</div>';
	}
?>

</div>
</div>
</div>
