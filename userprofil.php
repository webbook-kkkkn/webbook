<?php
$query = 'select * from users where id_user="' . $params[0] . '" limit 1;';
$action = 'userprofil';
if($q = $db->query($query))
{
	$q_array = $q->fetch_array(MYSQLI_ASSOC);
	$nazwa = $q_array['username'];
	$email = $q_array['email'];
	$id = $q_array['id_user'];
}

if($zalogowany)
{
	$like_query = 'SELECT count(*) as czydal FROM oceny where dla_kogo="'.$params[0].'" and od_kogo="'.$_SESSION['id_uzytkownika'].'" and ocena="1"';
	$dislike_query = 'SELECT count(*) as czydal FROM oceny where dla_kogo="'.$params[0].'" and od_kogo="'.$_SESSION['id_uzytkownika'].'" and ocena="-1"';
	$z_l = $db->query($like_query);
	$likes = $z_l->fetch_array();
	$z_dl = $db->query($dislike_query);
	$dislikes = $z_dl->fetch_array();
	$dal_like = $likes['czydal'];
	$dal_dislike = $dislikes['czydal'];
	$dal = $dal_like + $dal_dislike;
}
if(isset($_FILES['avatar'])){
	 $file_name = $_FILES['avatar']['name'];
	 $file_tmp =$_FILES['avatar']['tmp_name'];
	 $file_name_array = explode('.',$file_name);
	 $file_name_extension = end($file_name_array);
	 $file_ext=strtolower($file_name_extension);
	 $avatar_dir = 'images/avatary';
	 $file = $avatar_dir . '/' . $nazwa . '.' . $file_ext;
	 move_uploaded_file($file_tmp,$file);
	 chmod($file, 0666);
	 if($file_ext != 'png'){
		 imagepng(imagecreatefromstring(file_get_contents($file)), $avatar_dir . '/' . $nazwa . '.png');
		 chmod($avatar_dir . '/' . $nazwa . '.png', 0666);
		 unlink($file);
	 }
	 $_SESSION['avatar_name'] = $nazwa . '.png';
	 header("Location: /webbook/userprofil/" . $id);

	 //move_uploaded_file($file_tmp,'images/avatary/'.$file_name);
}

if(isset($params[1]) && $params[1] == 'addlike')
{
	if($dal == 0)
	{
		$query_likes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", 1)';
		$db->query($query_likes);
	}
	else
	{
		$db->query('delete from oceny where od_kogo="' . $_SESSION['id_uzytkownika'] . '" and dla_kogo="' . $params[0] . '";');
		if($dal_dislike == 1)
		{
			$query_likes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", 1)';
			$db->query($query_likes);
		}
	}

	header('Location: /webbook/userprofil/' . $params[0]);
}

if(isset($params[1]) && $params[1] == 'addunlike')
{
	if($dal == 0)
		{
			$query_unlikes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", -1)';
			$db->query($query_unlikes);
		}
		else
		{
				$db->query('delete from oceny where od_kogo="' . $_SESSION['id_uzytkownika'] . '" and dla_kogo="' . $params[0] . '";');
				if($dal_like == 1)
				{
				$query_unlikes = 'INSERT INTO oceny VALUES(null, "'.$params[0].'", "'.$_SESSION['id_uzytkownika'].'", -1)';
				$db->query($query_unlikes);
			}
		}

		header('Location: /webbook/userprofil/' . $params[0]);
}

?>
<article class="ui piled segment">
<div class="ui two column stackable grid">
<!--<div style="float:left; text-align: center;">-->
	<div class="four wide column">
	<div class="sub header">Nazwa użytkownika: <b><?php echo $nazwa; ?></b></div>
	<div class="sub header">Adres e-mail: <b><?php echo $email; ?></b></div>
<?php
if($zalogowany)
echo '<form action="./edytujuser/'.$params[0].'" method="post" class="ui reply form">
    <button class="ui inverted red labeled submit icon button" type="submit">
      <i class="icon pencil"></i> Edytuj dane
    </button>
<br />
</form>';
?>
	<div class="sub header">Avatar użytkownika:</div>
	<?php if($id == @$_SESSION['id_uzytkownika']){
		echo '<img class="ui small image centered"  onclick="$(\'.ui.modal\').modal(\'show\');" src="' . getUserAvatar($nazwa) . '"><br />';
	} else {
		echo '<img class="ui small image centered" src="' . getUserAvatar($nazwa) . '"><br />';
	}
	?>


<?php
if($zalogowany && $dal_like == 0)//zalogowany bez like
{
	echo '<a class="ui labeled button" tabindex="0" href="./userprofil/'.$params[0].'/addlike">
	<div class="ui grey button">
    <i class="heart icon"></i> Lubi
  </div>
  <div class="ui basic grey left pointing label"">
	';
}
else if($zalogowany) //zalogowany z like
{
	echo '<a class="ui labeled button" tabindex="0" href="./userprofil/'.$params[0].'/addlike">
	<div class="ui red button">
		<i class="heart icon"></i> Polubiono
	</div>
	<div class="ui basic red left pointing label"">
';
}
else //niezalogowany
{
	echo '<a class="ui disabled labeled button" tabindex="0" href="./userprofil/'.$params[0].'/addlike">
	<div class="ui grey button">
		<i class="heart icon"></i> Lubi
	</div>
	<div class="ui basic grey left pointing label"">
';
}

	//ilosc łapek w gore
	$lajki = $db->query('select count(*)  as c from oceny where dla_kogo="' . $params[0] . '" and ocena="1" limit 1;');
	$i = $lajki->fetch_array(MYSQLI_ASSOC);
	$ilosc_lajkow = $i['c'];
	echo $ilosc_lajkow;
	?>
</div>
</a>
<br />
<br />
<?php
if($zalogowany && $dal_dislike == 0)
	{
		echo '<a class="ui labeled button" tabindex="0" href="./userprofil/'.$params[0].'/addunlike">
		<div class="ui grey button">
			<i class="thumbs down icon"></i> Nie lubi
		</div>
		<div class="ui basic grey left pointing label"">
';
	}
	else if($zalogowany)
	{
		echo '<a class="ui labeled button" tabindex="0" href="./userprofil/'.$params[0].'/addunlike">
		<div class="ui red button">
			<i class="thumbs down icon"></i> Nie polubiono
		</div>
		<div class="ui basic red left pointing label"">
';

	}
else {
	echo '<a class="ui disabled labeled button" tabindex="0" href="./userprofil/'.$params[0].'/addunlike">
	<div class="ui grey button">
		<i class="thumbs down icon"></i> Nie lubi
	</div>
	<div class="ui basic grey left pointing label"">
';

}
	//ilosc lapek w dol
	$unlajki = $db->query('select count(*)  as u from oceny where dla_kogo="' . $params[0] . '" and ocena="-1" limit 1;');
	$u = $unlajki->fetch_array(MYSQLI_ASSOC);
	$ilosc_unlajkow = $u['u'];
	echo $ilosc_unlajkow;
	?>
</div>
</a>
<br />
<?php
if($zalogowany)
echo '<form action="./napisz/'.$params[0].'" method="post" class="ui reply form">
<br />
    <button class="ui blue labeled submit icon button" type="submit">
      <i class="icon send"></i> Wyślij wiadomość
    </button>
<br />
</form>';
?>
<br />
<?php
if($zalogowany)
echo '<form action="./zglos/'.$params[0].'" method="post" class="ui reply form">
    <button class="ui inverted red labeled submit icon button" type="submit">
      <i class="icon pencil"></i> Zgłoś użytkownika
    </button>
<br />
</form>';
?>
</div>
<div class="twelve wide column">
<?php include 'userogloszenia.php' ?>
</div>
</div>
<div style="clear:both;"></div>

<div class="ui comments" style="margin-top: 50px; max-width: 100%;">
<h3 class="ui dividing header">Komentarze użytkownika</b></h3>
<?php

showCommentsRecursively($params[0]);

if($zalogowany)
{
echo '<form method="POST" class="ui reply form" action="./dodaj_komentarz/'.$params[0].'">';
echo '<div class="required field">';
	echo '<div class="field">';
	echo '<textarea name="opis"></textarea>';
	echo '</div>';
	echo '<button type="submit" class="ui labeled blue icon button"><i class="save icon"></i>Dodaj komentarz</button>';
echo '</form>';
}
?>
</div>
</article>
<div class="ui modal">
	<form method="POST" class="ui form" enctype="multipart/form-data">
	<div class="ui header">
		Zmień avatar
	</div>
	<div class="content">
		<input name="avatar" type="file">

		</form>
	</div>
	<div class="ui action">
		<button type="submit "class="ui orange button">Zmień Avatar</button>
</div>
</div>
