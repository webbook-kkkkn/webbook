<form method="post" class="ui form">
  <div class="inline fields">
      <div class="eight wide field">
        <input type="text" name="fraza" placeholder="Fraza" />
      </div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="add icon"></i>
          Dodaj
        </button>
      </div>
  </div>
</form>

<?php
if(!empty($params[0]))
{
  $db->query('delete from obserwowane where id=' . $params[0]);
  header("Location: /webbook/obserwowane/");
}
echo '<h1 style="ui header">Obserwowane</h1>';
if(isset($_POST['fraza']))
{
  $db->query('insert into obserwowane values(null, "'.$_POST['fraza'].'", '.$_SESSION['id_uzytkownika'].')');
  header("Location: /webbook/obserwowane/");
}
$query_obserwowane = 'select * from obserwowane where id_uzytkownika=' . $_SESSION['id_uzytkownika'];
$obserwowane = $db->query($query_obserwowane);
if($obserwowane->num_rows > 0)
{
  echo '<table class="ui table">';
  foreach ($obserwowane as $o)
  {
    echo '<tr>';
    echo '<td>' . $o['fraza'] . '</td><td><a class="ui red labeled icon button" href="obserwowane/' . $o['id'] . '"><i class="trash icon"></i>usuń</a></td>';
    echo '</tr>';
  }
  echo '</table>';
}
?>
