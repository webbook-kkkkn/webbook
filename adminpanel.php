<h1 style="ui header">Panel Administracyjny</h1>
<?php

?>
<article class="ui piled segment">
<h3 style="ui header">Zarządzanie gatunkami książek</h3>
<h4 style="ui header">Dodaj gatunek książki</h4>

<form method="post" class="ui form" action="./adminpanel/add_category">
  <div class="inline fields">
      <div class="eight wide field">
        <input type="text" name="nazwa_kategorii" placeholder="Nazwa gatunku" />
      </div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="add icon"></i>
          Dodaj nowy gatunek
        </button>
      </div>
  </div>
</form>

<?php
if(isset($params[0]) && $params[0] == 'add_category')
	{

		$query_add_kategoria = 'INSERT INTO kategorie VALUES(null, "'.$_POST['nazwa_kategorii'].'")';
		$db->query($query_add_kategoria);
	header('Location: /webbook/adminpanel');
	}
?>
<h4 style="ui header">Usuń gatunek</h4>

<?php
$q_s_kategorie = 'select * from kategorie;';
$q_kategorie = $db->query($q_s_kategorie);

?>
<form method="post" class="ui form" action="./adminpanel/del_category">
  <div class="inline fields">
  <div class="eight wide field">
      <select class="ui dropdown" name="wybor_kategoria">
	  <option disabled selected value>Wybierz gatunek do usunięcia</option>
		<?php
			foreach($q_kategorie as $k)
			{
				echo '<option value="'.$k['id_kategoria'].'"';
				echo '>'.$k['nazwa_kategorii'].'</option>';
			}
		?>
	</select>
      <div class="field">
        <button type="submit" class="ui red labeled icon button">
          <i class="delete icon"></i>
          Usuń gatunek
        </button>
      </div>
  </div>
  </div>
</form>
</article>
<?php
if(isset($params[0]) && $params[0] == 'del_category')
	{
		//print_r($_POST['wybor_kategoria']);
		$db->query('delete from kategorie where id_kategoria = "' . $_POST['wybor_kategoria']. '"');
		header('Location: /webbook/adminpanel');
	}
?>
<?php
$q_s_users = 'select * from users order by username;';
$q_users = $db->query($q_s_users);

?>
<article class="ui piled segment">
<h3 style="ui header">Zarządzanie użytkownikami</h3>
<h4 style="ui header">Blokowanie/odblokowywanie użytkownika</h4>
<form method="post" class="ui form" action="./adminpanel/manage_user">
  <div class="inline fields">
  <div class="wide field">
      <select class="ui dropdown" name="wybor_usera">
	  <option disabled selected value>Wybierz użytkownika do zarządzania</option>
		<?php
			foreach($q_users as $u)
			{
				if($u['admin']!= 1)
				{
					echo '<option value="'.$u['id_user'].'"';
					echo '>'.$u['username'].'</option>';
				}
			}
		?>
	</select>
	<div class="field">
	<select class="ui dropdown" name="wybor_akcji">
		<option disabled selected value>Wybierz akcję</option>
		<option value="0">Odblokuj użytkownika</option>
		<option value="-1">Zablokuj użytkownika</option>
		<option value="1">Przyznaj uprawnienia administratora</option>
	</select>
	</div>
      <div class="field">
        <button type="submit" class="ui green labeled icon button">
          <i class="pencil icon"></i>
          Zarządzaj użytkownikiem
        </button>
      </div>
  </div>
  </div>
</form>
<?php
if(isset($params[0]) && $params[0] == 'manage_user')
	{
		//print_r($_POST['wybor_akcji']);
		$db->query('update users set admin ='.$_POST['wybor_akcji'].' where id_user= "' . $_POST['wybor_usera']. '"');
		header('Location: /webbook/adminpanel');
	}
?>
<h4 style="ui header">Usuń użytkownika</h4>
<form method="post" class="ui form" action="./adminpanel/del_user">
  <div class="inline fields">
  <div class="eight wide field">
      <select class="ui dropdown" name="wybor_usera">
	  <option disabled selected value>Wybierz użytkownika do usunięcia</option>
		<?php
			foreach($q_users as $u)
			{
				if($u['admin']!= 1)
				{
					echo '<option value="'.$u['id_user'].'"';
					echo '>'.$u['username'].'</option>';
				}
			}
		?>
	</select>
      <div class="field">
        <button type="submit" class="ui red labeled icon button">
          <i class="delete icon"></i>
          Usuń użytkownika
        </button>
      </div>
  </div>
  </div>
</form>
<?php
if(isset($params[0]) && $params[0] == 'del_user')
	{
		//print_r($_POST['wybor_kategoria']);
		$db->query('delete from users where id_user = "' . $_POST['wybor_usera']. '"');
		header('Location: /webbook/adminpanel');
	}
?>
<h4 style="ui header">Zmiana hasła użytkownika</h4>
<form method="post" class="ui form" action="./adminpanel/change_password">
  <div class="inline fields">
  <div class="eight wide field">
      <select class="ui dropdown" name="wybor_usera">
	  <option disabled selected value>Wybierz użytkownika, któremu chcesz zmienić hasło</option>
		<?php
			foreach($q_users as $u)
			{
				if($u['admin']!= 1)
				{
					echo '<option value="'.$u['id_user'].'"';
					echo '>'.$u['username'].'</option>';
				}
			}
		?>
	</select>
		<div class="field">
			<input type="password" name="haslo" placeholder="Podaj nowe hasło" />
		</div>
		<div class="field">
			<input type="password" name="haslo2" placeholder="Powtórz nowe hasło" />
		</div>
      <div class="field">
        <button type="submit" class="ui blue labeled icon button">
          <i class="pencil icon"></i>
          Zmień hasło użytkownikowi
        </button>
      </div>
  </div>
  </div>
</form>
</article>
<?php
if(isset($params[0]) && $params[0] == 'change_password')
	{
		if($_POST['haslo']==$_POST['haslo2']){
			$password = md5($_POST['haslo']);
		//print_r($_POST['wybor_kategoria']);
			$db->query('update users set password="'.$password.'" where id_user = "' . $_POST['wybor_usera']. '"');
			echo '<script> alert("Hasło zostało zmienione")</script>';
			//header('Location: /tablica/adminpanel');
			
		}
		else
		{
				//echo '<h1>Hasła się różnią!</h1>';
				echo '<script> alert("Podane hasła do siebie nie pasują. Hasło użytkownika nie zostało zmienione")</script>';
		}
	}
?>
<article class="ui piled segment">
<h3 style="ui header">Zgłoszenia</h3>
<?php
$zgloszenia = 'select zgloszenia.*, users.username from zgloszenia join users on zgloszenia.zgloszony=users.id_user';
$z = $db->query($zgloszenia);
?>
<br />
<table class="ui celled table">
  <thead>
    <tr>
	<th>Nick</th>
    <th>Powód</th>
    <th>Czy zrobione</th>
  </tr>
  </thead>
  <?php
  foreach($z as $zg)
  {
	if($zg['czy_zalatwione']==0){
		echo '<tr>';
	}
	else
	{
			echo '<tr class="active">';
	}
	echo '<td data-label="Nick">'.$zg['username'].'</td>';
	echo '<td data-label="Powód">'.$zg['powod'].'</td>';
	if($zg['czy_zalatwione']==1)
		{
			echo '<td data-label="Czy zrobione"><i class="check icon"></i></td>';
		}
	else
	{
		echo '<td data-label="Czy zrobione"><a href="./adminpanel/zrobione/'.$zg['id_zgloszenia'].'"><i class="close icon"></i></a></td>';
	}
	echo '</tr>';
	}
  ?>
 </table>
</article>
<?php
if(isset($params[0]) && $params[0] == 'zrobione')
	{
		//print_r($_POST['wybor_kategoria']);
		$db->query('update zgloszenia set czy_zalatwione="1" where id_zgloszenia='.$params[1]);
		header('Location: /webbook/adminpanel');
	}
?>

