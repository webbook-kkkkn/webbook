-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 08 Maj 2019, 17:45
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `webbook`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie`
--

CREATE TABLE `kategorie` (
  `id_kategoria` int(11) NOT NULL,
  `nazwa_kategorii` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `kategorie`
--

INSERT INTO `kategorie` (`id_kategoria`, `nazwa_kategorii`) VALUES
(1, 'Fantastyka, horror'),
(3, 'Kuchnia i diety'),
(4, 'Poradniki'),
(5, 'Biografie'),
(6, 'Specjalistyczne'),
(7, 'Kryminał, sensacja, thriller'),
(700, 'Inne');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `komentarze`
--

CREATE TABLE `komentarze` (
  `id_komentarzu` int(11) NOT NULL,
  `id_nadrzednego` int(11) DEFAULT NULL,
  `data_wystawienia` datetime DEFAULT NULL,
  `dla_kogo` int(11) DEFAULT NULL,
  `kto_dal` int(11) DEFAULT NULL,
  `tresc_komentarzu` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `komentarze`
--

INSERT INTO `komentarze` (`id_komentarzu`, `id_nadrzednego`, `data_wystawienia`, `dla_kogo`, `kto_dal`, `tresc_komentarzu`) VALUES
(1, NULL, '2019-03-28 06:18:00', 1, 20, 'Testowy komentarz'),
(6, NULL, '2019-03-28 06:18:00', 1, 20, '123'),
(7, NULL, '2019-03-28 06:18:00', 1, 20, '345'),
(8, NULL, '2019-03-28 20:47:58', 29, 1, 'adsad'),
(9, NULL, '2019-03-28 20:48:23', 1, 1, 'wszytsko sprawnie, polecam'),
(10, NULL, '2019-03-28 20:57:37', 0, 1, 'TEST 1234'),
(22, NULL, '2019-03-28 21:05:46', 0, 1, 'testowy'),
(23, NULL, '2019-03-29 11:49:52', 0, 1, '123'),
(24, NULL, '2019-03-29 12:09:25', 0, 1, 'adkapldkpa'),
(25, 8, '2019-04-08 22:34:26', 29, 1, 'sdf fdgsdfg dsfg sdf g'),
(26, NULL, '2019-04-08 22:58:08', 29, 1, 'Super, gut, alles klar! :)'),
(27, NULL, '2019-04-08 22:58:27', 29, 1, '6 sterne, polecam very much'),
(28, 25, '2019-04-08 22:58:54', 29, 1, 'Nie byłbym taki pewny, ja się przejechałem, nie polecam.'),
(29, 26, '2019-04-08 23:05:14', 29, 1, 'Das ist odpowiedź na alles klar'),
(30, 1, '2019-04-08 23:08:45', 1, 1, 'Testowa odpowiedź'),
(31, NULL, '2019-04-08 23:09:34', 1, 1, '567'),
(32, NULL, '2019-04-08 23:24:04', 1, 1, 'Nowy komentarz'),
(33, 26, '2019-04-08 23:24:44', 29, 1, 'ja, alles git'),
(34, 26, '2019-04-08 23:26:03', 29, 29, 'Danke');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `obserwowane`
--

CREATE TABLE `obserwowane` (
  `id` int(11) NOT NULL,
  `fraza` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `id_uzytkownika` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `obserwowane`
--

INSERT INTO `obserwowane` (`id`, `fraza`, `id_uzytkownika`) VALUES
(2, 'auto', 29),
(3, 'koń', 29),
(4, 'komputer', 29);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `oceny`
--

CREATE TABLE `oceny` (
  `id_oceny` int(11) NOT NULL,
  `dla_kogo` int(11) DEFAULT NULL,
  `od_kogo` int(11) DEFAULT NULL,
  `ocena` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `oceny`
--

INSERT INTO `oceny` (`id_oceny`, `dla_kogo`, `od_kogo`, `ocena`) VALUES
(33, 27, 1, -1),
(35, 1, 29, -1),
(36, 29, 29, 1),
(38, 20, 20, 1),
(39, 28, 1, 1),
(48, 29, 1, 1),
(51, 1, 1, 1),
(64, 11, 30, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `oceny_ksiazki`
--

CREATE TABLE `oceny_ksiazki` (
  `id_oceny` int(11) NOT NULL,
  `id_ksiazki` int(11) NOT NULL,
  `id_usera` int(11) NOT NULL,
  `ocena` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `oceny_ksiazki`
--

INSERT INTO `oceny_ksiazki` (`id_oceny`, `id_ksiazki`, `id_usera`, `ocena`) VALUES
(11, 11, 30, 1),
(14, 11, 1, 1),
(15, 10, 1, 1),
(16, 2, 1, 1),
(19, 1, 23, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ogloszenie`
--

CREATE TABLE `ogloszenie` (
  `id_ogloszenie` int(11) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `opis` text COLLATE utf8_polish_ci,
  `cena` float DEFAULT NULL,
  `data_wystawienia` datetime DEFAULT NULL,
  `fk_id_kategoria` int(10) NOT NULL,
  `fk_id_user` int(10) NOT NULL,
  `miasto` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '-',
  `fk_id_wojewodztwa` int(11) DEFAULT '17'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ogloszenie`
--

INSERT INTO `ogloszenie` (`id_ogloszenie`, `nazwa`, `opis`, `cena`, `data_wystawienia`, `fk_id_kategoria`, `fk_id_user`, `miasto`, `fk_id_wojewodztwa`) VALUES
(1, 'Początek', 'Robert Langdon, profesor Uniwersytetu Harvarda, specjalista w dziedzinie ikonologii religijnej i symboli, przybywa do Muzeum Guggenheima w Bilbao, gdzie ma dojść do ujawnienia odkrycia, które „na zawsze zmieni oblicze nauki”. Gospodarzem wieczoru jest Edmond Kirsch, czterdziestoletni miliarder i futurysta, którego oszałamiające wynalazki i śmiałe przepowiednie zapewniły mu rozgłos na całym świecie. Kirsch, który dwadzieścia lat wcześniej był jednym z pierwszych studentów Langdona na Harvardzie, planuje ujawnić informację, która będzie stanowić odpowiedź na fundamentalne pytania dotyczące ludzkiej egzystencji.', 40, '2019-05-02 10:03:50', 2, 1, 'Dan Brown', 1),
(2, 'Hastag', 'Twoja paczka już na ciebie czeka! – brzmiała wiadomość, która wydawała się zwykłą pomyłką. Tesa nie spodziewała się żadnej przesyłki, niczego nie zamawiała w sieci – a nawet gdyby to zrobiła, z pewnością nie wybrałaby dostawy do paczkomatu. Jeśli nie musiała, nie wychodziła z domu. Postanowiła jednak sprawdzić tajemniczą przesyłkę – i okazało się to największym błędem, jaki kiedykolwiek popełniła. Wpadła bowiem w spiralę zdarzeń, która miała zupełnie odmienić jej życie…', 40, '2019-05-02 10:13:06', 2, 1, 'Remigiusz Mróz', 1),
(5, 'Jak mniej myśleć', 'Bycie nadwydajnym bywa naprawdę męczące, ale twój mózg – właśnie ten, który myśli zbyt wiele – to prawdziwy skarb. Jego subtelność, złożoność i szybkość, z jaką działa, są naprawdę zdumiewające, a pod względem mocy można go porównać z silnikiem Formuły 1! Potrzebuje kierowcy najwyższej klasy i toru na swoją miarę, by wykorzystać swój potencjał. ', 38, '2019-05-02 10:26:01', 4, 1, 'Petitcollin Christel', 1),
(6, 'Dziennik 29. Interaktywna gra książkowa ', 'Minął 28 tydzień tajnych prac wykopaliskowych, które miały przybliżyć ludzkość do poznania sekretu obcej cywilizacji. W 29 tygodniu stało się jednak coś niezwykłego! Zniknął cały zespół, pozostawiając po sobie jedynie dziennik. Twoim zadaniem będzie rozwikłanie tej tajemnicy. Rozwiązuj zagadki i łamigłówki, aby zdobyć kolejne klucze i móc kontynuować rozgrywkę. Dziennik 29 to interaktywna książka, która jest równocześnie grą pełną niespodzianek. Czy odważysz się podjąć wyzwanie?', 30, '2019-05-02 10:30:17', 7, 1, 'Chassapakis Dimitris', 1),
(7, 'Becoming. Moja historia', 'Osobiste wspomnienia najbardziej lubianej Pierwszej Damy Stanów Zjednoczonych.', 45, '2019-05-02 10:34:48', 5, 1, 'Obama Michelle', 1),
(8, 'Sapiens. Od zwierząt do bogów', 'Całościowa historia człowieka - od ukrywania się w jaskiniach do podróży na Księżyc i inżynierii genetycznej.', 60, '2019-05-02 10:36:18', 7, 1, 'Harari Yuval Noah', 1),
(9, 'Insulinooporność. Szybkie dania ', 'Pierwsza pomoc dietetyczna dla insulinoopornych.\r\nJadłospisy na 3 miesiące\r\nPonad 200 przepisów, także w wersjach wegańskich i wegetariańskich\r\n5 podstawowych produktów w każdym przepisie\r\nCzas przygotowania: do 20 minut', 40, '2019-05-02 10:37:59', 3, 1, 'Makarowska Magdalena', 1),
(10, 'Emigracja', 'To książka o młodym człowieku, który po ukończeniu małomiasteczkowego liceum decyduje się na wyjazd zarobkowy do Wielkiej Brytanii. W charakterystyczny „pastowy” sposób opowiada o jednym z najważniejszych polskich zjawisk społecznych ostatnich dekad, które nadal nie zostało w wyczerpujący sposób przedstawione w polskiej literaturze. To opowieść o dorastaniu w powiatowej Polsce, obfitującej w przygody trasie do Londynu i samym życiu emigranta.', 35, '2019-05-02 10:39:38', 7, 1, 'Malcolm XD', 1),
(11, 'Java. Efektywne programowanie', 'Poznaj najlepsze praktyki programowania z użyciem platformy Java.', 80, '2019-05-02 10:40:51', 6, 1, 'Bloch Joshua', 1),
(12, 'Kasacja', 'Syn biznesmena zostaje oskarżony o zabicie dwóch osób. Sprawa wydaje się oczywista. Potencjalny winowajca spędza bowiem 10 dni zamknięty w swoim mieszkaniu w towarzystwie ciał zamordowanych osób.\r\n\r\nSprawę prowadzi Joanna Chyłka, pracująca dla bezwzględnej warszawskiej korporacji. Nieprzebierająca w środkach prawniczka, która zrobi wszystko, by odnieść zwycięstwo w batalii sądowej. Pomaga jej młody, zafascynowany przełożoną aplikant Kordian Oryński. Czy jednak wspólnie zdołają doprowadzić sprawę do szczęśliwego finału?\r\n\r\nTymczasem ich klient zdaje się prowadzić własną grę, której reguły zna tylko on sam. Nie przyznaje się do winy, ale też nie zaprzecza, że jest mordercą.\r\n\r\nDwoje prawników zostaje wciągniętych w wir manipulacji, który sięga dalej, niż mogliby przypuszczać.', 29, '2019-05-05 08:30:58', 2, 1, 'Remigiusz Mróz', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `avatar_name` varchar(255) COLLATE utf8_polish_ci DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id_user`, `username`, `email`, `password`, `admin`, `avatar_name`) VALUES
(1, 'admin', 'admin@admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'default.png'),
(20, 'Kosek', 'kosektomasz96@gmail.com', 'b849db7f2c7b1439fcfcaa47c6c0f520', 1, 'barca.png'),
(22, 'Test', 'test@wp.pl', '098f6bcd4621d373cade4e832627b4f6', -1, 'default.png'),
(23, 'Sanderka', 'sandeka@sandra.pl', '21232f297a57a5a743894a0e4a801fc3', 0, 'san.png'),
(24, 'simple', 'simple@simple.com', '8dbdda48fb8748d6746f1965824e966a', 0, 'default.png'),
(25, 'szkola', 'szkola@wp.pl', 'be8287c6f4dce7ff1c93c8d94b3a1032', 0, 'default.png'),
(27, 'Adminek', 'admin@adminek.pl', '202cb962ac59075b964b07152d234b70', 0, 'default.png'),
(29, 'krystian', 'krystian.komor@gmail.com', 'c75b6bdde1d90eb183d6fad61bb8da56', 1, 'default.png'),
(30, 'magda123', 'magdalena.kedzia3@gmail.com', '96626642cd0fcabd85a58a2b11ea6d12', 0, 'default.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wiadomosci`
--

CREATE TABLE `wiadomosci` (
  `id_wiadomosci` int(11) NOT NULL,
  `tresc_wiadomosci` text COLLATE utf8_polish_ci,
  `wiadomosc_od` int(11) DEFAULT NULL,
  `wiadomosc_do` int(11) DEFAULT NULL,
  `przeczytane` tinyint(1) DEFAULT NULL,
  `data_wiadomosci` datetime DEFAULT NULL,
  `temat` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `czyja_wiadomosc` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `wiadomosci`
--

INSERT INTO `wiadomosci` (`id_wiadomosci`, `tresc_wiadomosci`, `wiadomosc_od`, `wiadomosc_do`, `przeczytane`, `data_wiadomosci`, `temat`, `czyja_wiadomosc`) VALUES
(33, 'hi', 1, 21, 1, '2019-04-02 16:49:07', '', 1),
(34, 'test', 1, 1, 1, '2019-04-02 16:51:00', '', 1),
(35, 'hi', 1, 21, 1, '2019-04-02 16:54:48', '', 1),
(36, 'siemka', 1, 30, 1, '2019-04-02 16:56:39', '', 1),
(37, 'hi', 21, 1, 1, '2019-04-02 16:59:59', '', 1),
(38, 'co tam?', 1, 21, 1, '2019-04-02 17:07:43', '', 1),
(39, 'wiadomosc', 1, 30, 1, '2019-04-02 17:11:28', '', 1),
(40, 'hihihi', 1, 30, 1, '2019-04-02 17:12:01', '', 1),
(41, 'siema', 30, 1, 1, '2019-04-02 17:21:41', '', 1),
(42, 'elo', 30, 30, 1, '2019-04-02 17:23:06', '', 1),
(43, 'elo', 30, 1, 1, '2019-04-02 17:23:56', '', 1),
(44, 'siema', 1, 20, 1, '2019-04-02 17:33:46', '', 1),
(45, 'test', 20, 30, 1, '2019-04-02 17:38:13', '', 1),
(46, 'Znaleziono oberwowane ogłoszenie. <a href=\"wyswietl/58\">małe auto</a>', 1, 29, 1, '2019-04-02 20:44:48', 'Obserwowane', 1),
(47, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/56\">małe auto</a>', 1, 29, 1, '2019-04-02 20:45:53', 'Obserwowane', 1),
(48, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/59\">komputer</a>', 1, 29, 1, '2019-04-02 21:04:26', 'Obserwowane', 1),
(49, 'Dodano kowy komentarz do Twojego <a href=\"./userprofil/1\">profilu.</a>', 1, 1, 1, '2019-04-08 23:24:04', 'Nowy komentarz', 1),
(50, 'Dodano kowy komentarz do Twojego <a href=\"./userprofil/29\">profilu.</a>', 29, 29, 1, '2019-04-08 23:24:44', 'Nowy komentarz', 1),
(51, 'Dodano kowy komentarz do Twojego <a href=\"./userprofil/29\">profilu.</a>', 29, 29, 1, '2019-04-08 23:26:03', 'Nowy komentarz', 1),
(52, 'Odpisuję.', 1, 29, 1, '2019-04-09 00:04:18', '', 1),
(53, 'super. cieszę się bardzo', 29, 1, 1, '2019-04-09 00:15:19', '', 1),
(54, 'bla bla bla', 29, 1, 1, '2019-04-09 00:38:20', NULL, 1),
(55, 'działa?', 29, 1, 1, '2019-04-09 00:38:36', NULL, 1),
(56, 'owszem, bez problemów\r\ni bez zarzutów\r\n:)', 1, 29, 1, '2019-04-09 00:39:02', NULL, 1),
(57, '', 1, 1, 1, '2019-04-22 09:07:30', NULL, 1),
(63, 'Czy ogloszenie http://localhost/tablica/szczegoly/59/ jest nadal dostępne?', 1, 29, 0, '2019-04-22 15:48:44', '', 1),
(64, 'Czy ogloszenie http://localhost/tablica/szczegoly/59/ jest nadal dostępne?', 1, 29, 0, '2019-04-22 15:50:10', '', 1),
(65, 'Czy ogloszenie http://localhost/tablica/szczegoly/38/ jest nadal dostępne?', 1, 1, 1, '2019-04-22 15:50:25', '', 1),
(66, '', 1, 29, 0, '2019-04-22 16:13:59', NULL, 1),
(67, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/59\">komputer</a>', 1, 29, 0, '2019-04-24 12:40:10', 'Obserwowane', 1),
(68, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/60\">Komputer Hi-Tech2</a>', 1, 29, 0, '2019-04-24 14:11:18', 'Obserwowane', 1),
(69, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/62\">auto</a>', 1, 29, 0, '2019-04-24 14:16:06', 'Obserwowane', 1),
(70, 'Znaleziono oberwowane ogłoszenie. <a href=\"szczegoly/57\">małe auto</a>', 1, 29, 0, '2019-04-24 14:20:15', 'Obserwowane', 1),
(71, 'Znaleziono oberwowaną książkę. <a href=\"szczegoly/3\">Jak mniej myśleć. Dla analizujących bez końca i wysoko wrażliwych</a>', 1, 29, 0, '2019-05-02 10:17:02', 'Obserwowane', 1),
(72, 'Znaleziono oberwowaną książkę. <a href=\"szczegoly/3\">Jak mniej myśleć. Dla analizujących bez końca i wysoko wrażliwych</a>', 1, 29, 0, '2019-05-02 10:17:18', 'Obserwowane', 1),
(73, 'Znaleziono oberwowaną książkę. <a href=\"szczegoly/4\">Jak mniej myśleć. Dla analizujących bez końca i wysoko wrażliwych</a>', 1, 29, 0, '2019-05-02 10:19:07', 'Obserwowane', 1),
(74, 'Znaleziono oberwowaną książkę. <a href=\"szczegoly/5\">Jak mniej myśleć. Dla analizujących bez końca i wysoko wrażliwych</a>', 1, 29, 0, '2019-05-02 10:26:01', 'Obserwowane', 1),
(75, 'Znaleziono oberwowaną książkę. <a href=\"szczegoly/5\">Jak mniej myśleć. Dla analizujących bez końca i wysoko wrażliwych</a>', 1, 29, 0, '2019-05-02 10:26:54', 'Obserwowane', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wojewodztwa`
--

CREATE TABLE `wojewodztwa` (
  `id_wojewodztwa` int(10) NOT NULL,
  `nazwa_wojewodztwa` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wojewodztwa`
--

INSERT INTO `wojewodztwa` (`id_wojewodztwa`, `nazwa_wojewodztwa`) VALUES
(1, 'dolnośląskie'),
(2, 'kujawsko-pomorskie'),
(3, 'lubelskie'),
(4, 'lubuskie'),
(5, 'łódzkie'),
(6, 'małopolskie'),
(7, 'mazowieckie'),
(8, 'opolskie'),
(9, 'podkarpackie'),
(10, 'podlaskie'),
(11, 'pomorskie'),
(12, 'śląskie'),
(13, 'świętokrzyskie'),
(14, 'warmińsko-mazurskie'),
(15, 'wielkopolskie'),
(16, 'zachodniopomorskie'),
(17, 'nie podano');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zgloszenia`
--

CREATE TABLE `zgloszenia` (
  `id_zgloszenia` int(11) NOT NULL,
  `zglaszajacy` int(11) DEFAULT NULL,
  `zgloszony` int(11) DEFAULT NULL,
  `powod` text CHARACTER SET latin1,
  `czy_zalatwione` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zgloszenia`
--

INSERT INTO `zgloszenia` (`id_zgloszenia`, `zglaszajacy`, `zgloszony`, `powod`, `czy_zalatwione`) VALUES
(1, 20, 22, 'Awatar nieodpowiedni', 1),
(2, 1, 22, 'Avatar', 1),
(5, 1, 29, 'Obrazliwy login uzytkownika', 1),
(6, 1, 20, 'Spamowanie ogloszeniami', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`id_kategoria`);

--
-- Indeksy dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  ADD PRIMARY KEY (`id_komentarzu`);

--
-- Indeksy dla tabeli `obserwowane`
--
ALTER TABLE `obserwowane`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `oceny`
--
ALTER TABLE `oceny`
  ADD PRIMARY KEY (`id_oceny`);

--
-- Indeksy dla tabeli `oceny_ksiazki`
--
ALTER TABLE `oceny_ksiazki`
  ADD PRIMARY KEY (`id_oceny`);

--
-- Indeksy dla tabeli `ogloszenie`
--
ALTER TABLE `ogloszenie`
  ADD PRIMARY KEY (`id_ogloszenie`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeksy dla tabeli `wiadomosci`
--
ALTER TABLE `wiadomosci`
  ADD PRIMARY KEY (`id_wiadomosci`);

--
-- Indeksy dla tabeli `wojewodztwa`
--
ALTER TABLE `wojewodztwa`
  ADD PRIMARY KEY (`id_wojewodztwa`);

--
-- Indeksy dla tabeli `zgloszenia`
--
ALTER TABLE `zgloszenia`
  ADD PRIMARY KEY (`id_zgloszenia`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `id_kategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  MODIFY `id_komentarzu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT dla tabeli `obserwowane`
--
ALTER TABLE `obserwowane`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `oceny`
--
ALTER TABLE `oceny`
  MODIFY `id_oceny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT dla tabeli `oceny_ksiazki`
--
ALTER TABLE `oceny_ksiazki`
  MODIFY `id_oceny` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT dla tabeli `ogloszenie`
--
ALTER TABLE `ogloszenie`
  MODIFY `id_ogloszenie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT dla tabeli `wiadomosci`
--
ALTER TABLE `wiadomosci`
  MODIFY `id_wiadomosci` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT dla tabeli `wojewodztwa`
--
ALTER TABLE `wojewodztwa`
  MODIFY `id_wojewodztwa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `zgloszenia`
--
ALTER TABLE `zgloszenia`
  MODIFY `id_zgloszenia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
