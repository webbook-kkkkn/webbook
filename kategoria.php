<?php

	$kategoriaid = array_shift($params);

	$link_przedrostek = 'kategoria/' . $kategoriaid . '/';

	if(isset($params[0]))
	{
		$sort_array = explode(',', $params[0]);
		if($sort_array[0] == 'sortuj')
		{
			array_shift($params);
			$co = $sort_array[1];
			$jak = $sort_array[2];
			$query_order = $co . ' ' . $jak . ', ';
			$link_sortuj = 'sortuj,' . $co . ',' . $jak . '/';
		}
		else
		{
				$query_order = '';
				$link_sortuj = '';
		}
	}
	else
	{
		$query_order = '';
		$link_sortuj = '';
	}

	if(isset($_GET['filtruj']))
	{
		$link_filtruj = 'filtruj';
		foreach ($_GET as $k => $v) {
			$link_filtruj .= ',' . $v;
		}
		header("Location: /webbook/" . $link_przedrostek . $link_sortuj . $link_filtruj);
	}
	elseif(isset($params[0]))
	{
		$params[0] = urldecode($params[0]);
		$filtr_array = explode(',', $params[0]);
		if($filtr_array[0] == 'filtruj')
		{
			$link_filtruj = $params[0] . '/';
			array_shift($params);
			$cena_od = $filtr_array[1];
			$cena_do = $filtr_array[2];
			$wojewodztwo = $filtr_array[3];
			$miasto = $filtr_array[4];
			$dodano = $filtr_array[5];
			$query_where_array = array();
			if(!empty($cena_od) && !empty($cena_do))
			{
				$query_where_array[] = 'cena between ' . $cena_od . ' and ' . $cena_do;
			}
			else if(!empty($cena_od))
			{
				$query_where_array[] = 'cena >= ' . $cena_od;
			}
			//else if(!empty($cena_do) || $cena_do == 0)
			else if(!empty($cena_do))
			{
				$query_where_array[] = 'cena <= ' . $cena_do;
			}

			if(!empty($dodano))
			{
				$query_where_array[] = 'data_wystawienia >= "' . $dodano . '"';
			}

			if(!empty($miasto))
			{
				$query_where_array[] = 'miasto = "' . $miasto . '"';
			}

			if(!empty($wojewodztwo))
			{
				$query_where_array[] = 'wojewodztwa.nazwa_wojewodztwa = "' . $wojewodztwo . '"';
			}

			$query_where = implode(' and ', $query_where_array);
			if(count($query_where_array) > 0) $query_where = ' and ' . $query_where;
			//echo $query_where;
		}
		else
		{
				$query_where = '';
				$link_filtruj = '';
				$cena_od = '';
				$cena_do = '';
				$dodano = '';
				$wojewodztwo = '';
				$miasto = '';
				$link_filtruj = '';
		}
	}
	else
	{
		$query_where = '';
		$link_filtruj = '';
		$cena_od = '';
		$cena_do = '';
		$dodano = '';
		$wojewodztwo = '';
		$miasto = '';
		$link_filtruj = '';
	}

	$link_aktywna = $link_przedrostek . $link_sortuj . $link_filtruj;

		if($link_przedrostek=='kategoria/3/')
		{
			echo '<h1 style="ui header">Kategoria: AGD</h1>';
		}
		elseif($link_przedrostek=='kategoria/4/')
		{
			echo '<h1 style="ui header">Kategoria: Antyki</h1>';
		}
		elseif($link_przedrostek=='kategoria/5/')
		{
			echo '<h1 style="ui header">Kategoria: Chemia</h1>';
		}
		elseif($link_przedrostek=='kategoria/1/')
		{
			echo '<h1 style="ui header">Kategoria: Elektronika</h1>';
		}
		elseif($link_przedrostek=='kategoria/7/')
		{
			echo '<h1 style="ui header">Kategoria: Inne</h1>';
		}
		elseif($link_przedrostek=='kategoria/2/')
		{
			echo '<h1 style="ui header">Kategoria: Motoryzacja</h1>';
		}
		elseif($link_przedrostek=='kategoria/6/')
		{
			echo '<h1 style="ui header">Kategoria: Spożywcze</h1>';
		}

	$wszystkie_ogloszenia_query = 'select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where fk_id_kategoria="'. $kategoriaid . '" '.$query_where;
	//echo $wszystkie_ogloszenia_query;
	$wszystkie_ogloszenia = $db->query($wszystkie_ogloszenia_query);
	$wszystkie = $wszystkie_ogloszenia->num_rows;

	$ilosc_na_strone = 6;
	$ilosc_stron = ceil($wszystkie / $ilosc_na_strone);

	$strona = !empty($params[0]) && is_numeric($params[0]) ? $params[0] : 1;

	$ogloszenia_query = 'select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria=kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where fk_id_kategoria="'. $kategoriaid . '" '.$query_where.' order by ' . $query_order . 'data_wystawienia desc, id_ogloszenie desc limit ' . (($strona - 1) * $ilosc_na_strone) . ', ' . ($ilosc_na_strone);
	//echo $ogloszenia_query;
	$ogloszenia = $db->query($ogloszenia_query);

?>

<div class="ui  menu">
	<div class="ui labeled icon dropdown item">
		<div class="text">Sortuj</div>
		<i class="dropdown icon"></i>
		<div class="menu">
			<a href="<?php echo $link_przedrostek; ?>sortuj,cena,asc/<?php echo $link_filtruj; ?>" class="item">Cena rosnąco</a>
			<a href="<?php echo $link_przedrostek; ?>sortuj,cena,desc/<?php echo $link_filtruj; ?>" class="item">Cena malejąco</a>
			<a href="<?php echo $link_przedrostek; ?>sortuj,nazwa,asc/<?php echo $link_filtruj; ?>" class="item">Nazwa A-Z</a>
			<a href="<?php echo $link_przedrostek; ?>sortuj,nazwa,desc/<?php echo $link_filtruj; ?>" class="item">Nazwa Z-A</a>
			<a href="<?php echo $link_przedrostek; ?>sortuj,data_wystawienia,asc/<?php echo $link_filtruj; ?>" class="item">Data rosnąco</a>
			<a href="<?php echo $link_przedrostek; ?>sortuj,data_wystawienia,desc/<?php echo $link_filtruj; ?>" class="item">Data malejąco</a>
		</div>
	</div>
	<form style="ui form" action="<?php echo $link_aktywna; ?>" method="get">
	<div class="ui form">

		<div style="float:left;margin-right:1%;margin-left: 1%; text-align: center;">
			<label>Cena</label>
			<div class="field">
			<input type="number" name="cena_od" placeholder="od" value="<?php echo $cena_od; ?>" />
			</div>
			<div class="field">
			<input type="number" name="cena_do" placeholder="do" value="<?php echo $cena_do; ?>" />
			</div>
		</div>

		<div style="float:left;margin-right:1%; text-align: center;">
			<label>Lokalizacja</label>
			<div class="field">
				<select class="ui dropdown" name="wojewodztwo">
					<option value=""></option>
					<?php
						foreach($db->query('select * from wojewodztwa') as $w)
						{
							echo '<option value="'.$w['nazwa_wojewodztwa'].'"';
							if($wojewodztwo == $w['nazwa_wojewodztwa'])
								echo ' selected="selected"';
							echo '>'.$w['nazwa_wojewodztwa'].'</option>';
						}
					?>
				</select>
			</div>
		<!--</div>
		<div style="float:left;margin-right:1%;">-->
			<!--<label>Miejscowość</label>-->
			<div class="field">
				<input type="text" name="miejscowosc" placeholder="Miejscowość" value="<?php echo $miasto; ?>" />
			</div>
		</div>
		<div style="float:left;text-align: center;">
			<label>Dodano</label>
			<div class="field">
				<input type="date" name="dodano" placeholder="Dodano" value="<?php echo $dodano; ?>" />
			</div>
		</div>
		<input class="ui grey button" type="submit" name="filtruj" value="Filtruj" style="margin-left:7.5%;margin-top: 2.3%;"/>
		<p> </p>
		<div style="clear:both;"></div>
		</div>
	</form>
</div>
<?php

if($wszystkie > 0 && $strona <= $ilosc_stron)
{
		echo '<div class="ui two column stackable grid">';

		include 'ogloszenia.php';

		if($ilosc_stron > 1){
			echo '<div class="one column row">';
			echo '<div class="ui pagination menu grid container">';

			$poprzednia = $strona -1;
			$nastepna = $strona + 1;

			if($poprzednia > 0)
			{
				echo '<a href="' . $link_aktywna . $poprzednia .'" class="item left aligned">Poprzednia</a>';
			}
			else
			{
				echo '<div class="item left disabled aligned">Poprzednia</div>';
			}
			for($i = 0; $i < $ilosc_stron; $i++)
			{
				$a = ($i + 1);
				echo '<a href="' . $link_aktywna . $a . '" class="item one wide column';
				if($strona == $a) echo ' active';
				echo '">' . $a . '</a>&nbsp;';
			}

			if ($nastepna <= $ilosc_stron)
			{
				echo '<a href="'. $link_aktywna . $nastepna .'" class="item right aligned">Nastepna</a>';
			}
			else
			{
				echo '<div class="item disabled right aligned">Nastepna</div>';
			}

			echo '</div>';
			echo '</div>';
		}
		echo '</div>';
}
else
{
	echo '<div class="ui segment"><h1>Brak wyników</h1></div>';
}
?>
