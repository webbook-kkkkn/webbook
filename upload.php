<?php
$ds          = DIRECTORY_SEPARATOR;  //1

$storeFolder = 'uploads';   //2

if (!empty($_FILES)) {

    $tempFile = $_FILES['file']['tmp_name'];          //3

    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4

    $targetFile =  $targetPath. $_FILES['file']['name'];  //5

    move_uploaded_file($tempFile,$targetFile); //6

} else {
    session_start();
    if(isset($_SESSION['upload_id']) && $_SESSION['upload_id'] > 0) $storeFolder = 'images/ogloszenia/' . $_SESSION['upload_id'];
    $result  = array();

    $files = scandir($storeFolder);                 //1
    if ( false!==$files ) {
        foreach ( $files as $file ) {
            if ( '.'!=$file && '..'!=$file && $file !='thumbnail') {       //2
              $obj['name'] = $file;
              $obj['dataURL'] = $storeFolder . '/' . $file;
              $obj['size'] = filesize($storeFolder.$ds.$file);
              $result[] = $obj;
            }
        }
    }

    header('Content-type: text/json');              //3
    header('Content-type: application/json');
    echo json_encode($result);
}
?>
