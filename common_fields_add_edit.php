<?php

echo '<h1 style="ui header">Dodaj/edytuj książkę</h1>';
$q_s_kategorie = 'select * from kategorie;';
$q_kategorie = $db->query($q_s_kategorie);
$q_s_wojewodztwa = 'select * from wojewodztwa;';
$q_wojewodztwa = $db->query($q_s_wojewodztwa);
if(isset($_POST['tytul'])){
	$nazwa = $_POST['tytul'];
	$opis = $_POST['opis'];
	$cena = $_POST['cena'];
	$miasto = ucfirst($_POST['miasto']);
  if(!isset($data_wystawienia))
	 $data_wystawienia = date('Y-m-d-H-i-s');
	$id_kategorii = $_POST['wybor_kategoria'];
	$id_wojewodztwa = 1;
	$id_uzytkownika = $_SESSION['id_uzytkownika'];
  $query = sprintf($query_format, $nazwa, $opis, $cena, $data_wystawienia, $id_kategorii, $id_uzytkownika, $miasto, $id_wojewodztwa);
	//echo $query;
	if($db->query($query)){

		if($action == 'dodaj') $id = $db->insert_id;
		$dir = 'images/ogloszenia/' . $id;
		if(!is_dir($dir)) mkdir($dir, 0777);
		$source = 'uploads/';
		$destination = $dir . '/';
		$files = scandir($source);
		foreach ($files as $file) {
		  if (in_array($file, array(".",".."))) continue;
		  if (copy($source . $file, $destination . $file)) {
		    unlink($source . $file);
		  }
		}

		$obserwowane_query = 'select * from obserwowane';
		$obserwowane = $db->query($obserwowane_query);
		foreach($obserwowane as $o)
		{
			$wyniki = $db->query('select * from ogloszenie where id_ogloszenie="'.$id.'" and nazwa like "%' . $o['fraza'] . '%"');
			if($wyniki->num_rows > 0)
			{
				$w = $wyniki->fetch_assoc();
				$w_insert_query = 'insert into wiadomosci values(null, "Znaleziono oberwowaną książkę. <a href=\"szczegoly/'.$id.'\">'.$w['nazwa'].'</a>", 1, '.$o['id_uzytkownika'].', 0, "'.date('Y-m-d H:i:s').'", "Obserwowane", 1)';
				//echo $w_insert_query;
				$db->query($w_insert_query);
			}
		}
		header('Location: /webbook');
	}
} elseif($action == 'dodaj') {
  $nazwa = '';
  $opis = '';
  $cena = '';
  $miasto = '';
  //$id_wojewodztwa = 17;
}
$tekst_button = $action == 'dodaj' ? 'Dodaj książkę' : 'Zapisz zmiany';
?>

<form method="POST" class="ui form" enctype="multipart/form-data">

	<div class="required field">
	<label>Tytuł książki</label>
	<input name="tytul" type="text" value="<?=$nazwa?>" required/>
	</div>
	<div class="required field">
	<label>Autor</label>
	<input name="miasto" type="text" value="<?=$miasto?>" />
	</div>
	<div class="required field">
	<label>Gatunek</label>
	<select class="ui dropdown" name="wybor_kategoria">
		<?php
			foreach($q_kategorie as $k)
			{
				echo '<option value="'.$k['id_kategoria'].'"';
				if($id_kategorii == $k['id_kategoria']) echo ' selected="selected"';
				echo '>'.$k['nazwa_kategorii'].'</option>';
			}
		?>
	</select>
</div>
	<div class="required field">
	<label>Cena na okładce</label>
	<input name="cena" type="text" value="<?=$cena?>" />
	</div>
	<div class="field">
	<label>Zdjęcia</label>
	<div id="dropzone" class="dropzone"></div>
	</div>
	<div class="required field">
	<label>Opis</label>
	<textarea name="opis"><?=$opis?></textarea>
	</div>
	<button type="submit" class="positive ui labeled icon button"><i class="save icon"></i><?=$tekst_button?></button>
  <?php
    if($action == 'edytuj')
      echo '<button onclick="$(\'.ui.basic.modal\').modal(\'show\');" class="negative ui labeled icon button right floated" type="button"><i class="trash icon"></i> Usuń</a>';
  ?>
</form>

<div class="ui basic modal">
	<div class="ui icon header">
		<i class="trash alternate icon"></i>
		Usunąć?
	</div>
	<div class="content">
		<p>Operacja jest nieodwracalna.</p>
	</div>
	<div class="actions">
		<div class="ui green cancel inverted button">
			<i class="remove icon"></i>
			Nie
		</div>
		<a href="<?php echo 'usun/' . $id; ?>" class="ui red labeled icon ok button">
			<i class="trash alternate icon"></i>
			Tak
		</a>
	</div>
</div>

<script>
Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;
$("div#dropzone").dropzone({

	url: "upload.php",

	init: function() {
		dz = this;
        $.get('upload.php', function(data) {
            $.each(data, function(key,value){
                var mockFile = { name: value.name, dataURL: value.dataURL, size: value.size };
								dz.emit('addedfile', mockFile);
								//dz.emit('thumbnail', mockFile, mockFile.name);
								//dz.createThumbnailFromUrl(mockFile, mockFile.name);
								dz.createThumbnailFromUrl(mockFile,
    dz.options.thumbnailWidth, dz.options.thumbnailHeight,
    dz.options.thumbnailMethod, true, function (thumbnail) {
        dz.emit('thumbnail', mockFile, thumbnail);
    });
								dz.emit('complete', mockFile);
            });
        });
    },

	addRemoveLinks: true,
	removedfile: function(file) {
    var name = file.name;
    $.ajax({
        type: 'POST',
        url: 'delete_uploaded_file.php',
        data: "id="+name,
        dataType: 'html'
    });
		var _ref;
		return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
	}
});
</script>
