<h2 class="ui header">Moje ogłoszenia</h2>
<div class="ui grid">
<?php
	$wszystkie_ogloszenia = $db->query('select count(*) as c from ogloszenie where fk_id_user='.$_SESSION['id_uzytkownika'].';');
	$w = $wszystkie_ogloszenia->fetch_array(MYSQLI_ASSOC);
	$wszystkie = $w['c'];
	$ilosc_na_strone = 6;
	$ilosc_stron = ceil($wszystkie / $ilosc_na_strone);
	$id = $_SESSION['id_uzytkownika'];
	$strona = !empty($params[0]) ? $params[0] : 1;

	$ogloszenia = $db->query('select ogloszenie.*, users.username, users.email, kategorie.nazwa_kategorii, wojewodztwa.nazwa_wojewodztwa from ogloszenie join users on ogloszenie.fk_id_user = users.id_user join kategorie on ogloszenie.fk_id_kategoria = kategorie.id_kategoria join wojewodztwa on ogloszenie.fk_id_wojewodztwa=wojewodztwa.id_wojewodztwa where users.id_user LIKE"' . $id . '" order by data_wystawienia desc, id_ogloszenie desc limit ' . (($strona - 1) * $ilosc_na_strone) . ', ' . ($ilosc_na_strone));
	include 'ogloszenia.php';

	$poprzednia = $strona -1 ;
	$nastepna = $strona + 1;
?>
<div class="one column row" >
<div class="ui pagination menu grid container">
<?php
	if($poprzednia > 0){
		echo '<a href="mojeogloszenia/'. $poprzednia .'" class="item left aligned">Poprzednia</a>';
	}
	for($i = 0; $i < $ilosc_stron; $i++){
		$a = ($i + 1);
		echo '<a href="mojeogloszenia/' . $a . '" class="item one wide column">' . $a . '</a>&nbsp;';
	}
	if ($nastepna <= $ilosc_stron){
		echo '<a href="mojeogloszenia/'. $nastepna .'" class="item right aligned">Nastepna</a>';
	}
?>

</div>
</div>
</div>
